FROM python:latest
ENV PYTHONUNBUFFERED 1

RUN apt-get update && apt-get install -y \
	gettext
RUN apt-get autoremove -y && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY "config/python" "/config"

RUN pip install -r /config/requirements.txt

COPY "src" "/srv"

WORKDIR '/srv'

ENTRYPOINT [ "python" ]

CMD [ "corpus.py" ]