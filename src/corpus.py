# -*- coding: UTF-8 -*-

from flask import Flask, request, render_template, session
from flask_babel import Babel, gettext

from language_model import LanguageIdentification

from datetime import datetime
import settings

app = Flask(__name__)
babel = Babel(app)


@babel.localeselector
def get_locale():
    if request.args.get('lang'):
        session['lang'] = request.args.get('lang')
    return session.get('lang', 'be')


# Load default config and override config from an environment variable
app.config.update(dict(
    DEBUG=True,
    SECRET_KEY='sdfsdfsfdgdfg4fsd',
))
app.config.from_envvar('FLASKR_SETTINGS', silent=True)


def run(form_options, lang):
    tpl = """{}: {}
{}: {}
{}: {}
"""
    language_model = LanguageIdentification(top_k=int(gettext(form_options['selector'])))
    lang = language_model.predict_lang(form_options['input'].replace("\n"," "))

    tmp_lang = "\n"
    for label, score in zip(lang[0], lang[1]):
        tmp_lang += '{}: {}\n'.format(label.replace("__label__", ""), score)

    output_text = tpl.format(gettext("inp"), form_options['input'],
                             gettext("selector"), gettext(form_options['selector']),
                             gettext("result"), tmp_lang)
    return output_text


@app.route('/', methods=['GET', 'POST'])
def ServiceDemonstrator():
    lang = get_locale()
    form_options = {}
    _input = gettext('default input').replace('\\n', '\n')
    is_post = False
    output_text = ""
    if request.method == "POST":
        is_post = True
        _input = request.form.get("inputText")
        form_options['selector'] = request.form.get("selector")
        form_options['input'] = request.form.get("inputText")
        output_text = run(form_options, lang)
    return render_template('index.html', is_post=is_post, form_options=form_options, output_text=output_text,
                           _input=_input, lang=lang, now=datetime.utcnow())


if __name__ == '__main__':
    app.run(use_reloader=True, debug=True, host=settings.host, port=settings.port)
