import fasttext


class LanguageIdentification:

    def __init__(self, top_k):
        pretrained_lang_model = "lid.176.ftz"
        self.model = fasttext.load_model(pretrained_lang_model)
        self.top_k = top_k

    def predict_lang(self, text):
        predictions = self.model.predict(text, k=self.top_k) # returns top k matching languages
        return predictions